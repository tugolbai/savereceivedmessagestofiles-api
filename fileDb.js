const fs = require('fs');
const path = "./messages";

let data = [];
let filesName = [];

module.exports = {
    init() {
        try {
            this.save();
        } catch (e) {
            data = [];
        }
    },

    getItems() {
        return data;
    },

    addItem(item) {
        item.dateTime = new Date().toISOString();
        fs.writeFile((path + '/' + `${item.dateTime}.txt`), JSON.stringify(item), (err) => {
            if (err) {
                console.error(err);
            }
            console.log('File was saved!');
        });
        filesName = [];
        data = [];
        this.save()
    },

    save() {
        fs.readdir(path, (err, files) => {
            files.forEach(file => {
                filesName.push(file);
            });
            filesName.splice(0, filesName.length - 5);
            filesName.forEach(f => {
                fs.readFile((path + '/' + f), (err, dataFile) => {
                    if (err) {
                        console.error(err);
                    }
                    data.push(JSON.parse(dataFile));
                });
            })
        });

    }
};

const express = require('express');
const router = express.Router();
const db = require('../fileDb');

router.get('/', (req, res) => {
    const messages = db.getItems();
    return  res.send(messages);
});

router.post('/', (req, res) => {
    const message = {
        message: req.body.message
    }
    db.addItem(message);
    return  res.send('Created new file with name=' + message.dateTime);
});

module.exports = router;
